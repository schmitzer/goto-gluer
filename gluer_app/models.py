from flask_sqlalchemy import SQLAlchemy
from gluer_app import app
import os


app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/gluer'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=False, nullable=False)
    surname = db.Column(db.String(120), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), unique=False, nullable=False)
    impositions = db.relationship('Imposition', backref='user', lazy=True)

class Imposition(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstPhotoName = db.Column(db.String(120), unique=False, nullable=False)
    secondPhotoName = db.Column(db.String(120), unique=False, nullable=False)
    resultName = db.Column(db.String(120), unique=False, nullable=False)
    compressedFirstPhoto = db.Column(db.String(120), unique=False, nullable=False)
    compressedSecondPhoto = db.Column(db.String(120), unique=False, nullable=False)
    isDone = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    marks = db.relationship('Marks', backref='imposition', lazy=True)

class Marks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    imposition_id = db.Column(db.Integer, db.ForeignKey('imposition.id'), nullable=False)
    marksOnFirst = db.Column(db.ARRAY(db.Integer, dimensions=2))
    marksOnSecond = db.Column(db.ARRAY(db.Integer, dimensions=2))
    marksType = db.Column(db.String(120), unique=False, nullable=False)