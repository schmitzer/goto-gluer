from flask import render_template, request, redirect, url_for, flash
from gluer_app import app
from gluer_app.models import User, Imposition, Marks, db
import flask_restless
from wtforms import Form, PasswordField, StringField
from wtforms.validators import Length, InputRequired, Email, EqualTo


# class RegistrationForm(Form):
#     name = StringField('Name', [Length(min=4, max=25)], _name='name')
#     surname = StringField('Last name', [Length(min=4, max=25)], _name='surname')
#     email = StringField('Email Address', [Length(min=6, max=35), Email()], _name='email')
#     password = PasswordField('Password', [InputRequired(), EqualTo('confirm', message='Passwords must match'),
#                                           Length(min=8, max=40)], _name='password')
#     confirm = PasswordField('Repeat Password')


manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)


@app.route('/',  methods=('GET', 'POST'))
def start():
    if request.method == 'POST':
        new_user = User(name=request.form['name'], surname=request.form['surname'], email=request.form['email'],
                        password=request.form['password'])
        db.session.add(new_user)
        db.session.commit()
    return render_template('index.html')



