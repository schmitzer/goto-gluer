import vueSlider from 'vue-slider-component'

Vue.component('button-tool', {
	props :['source'],
  data: function () {
    return {
    }
  },
  template: `<button class="square"><img :src="source"> </button>`
});

var app = new Vue({
  el: '#app',
  components: {
    vueSlider
  },
  data: {
    "value": "1", 
    
  	navbarClass: {
  		display: ['-webkit-box', '-ms-flexbox', 'flex']
  	}, 
  	links: [{ 'href': '#', 'text': "main page"}, { 'href': '#', 'text':'second page'}, { 'href': '#', 'text':'third page'}],
  	tool_images:[ { "key": "1", "path": 'static/Arrow.svg'}, { "key": "2", "path": 'static/broken.svg'},
  	{ "key": "3", "path":'static/curve.svg'}, { "key": "4", "path": 'static/figures.svg'}, { "key": "5", "path": 'static/dot.svg'}]
  }
})
