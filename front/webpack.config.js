module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: "./script.js",
  output: {
    path: __dirname + "/dist",
    filename: "bundle.js"
  }
}