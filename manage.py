from gluer_app import app
from gluer_app.routes import manager
from gluer_app.models import db, Imposition, User, Marks

if __name__ == '__main__':
    db.create_all()
    manager.create_api(Imposition, methods=['GET', 'POST', 'DELETE', 'PUT'])
    manager.create_api(User, methods=['GET', 'POST', 'DELETE', 'PUT'])
    manager.create_api(Marks, methods=['GET', 'POST', 'DELETE', 'PUT'])
    app.run(debug=True)
